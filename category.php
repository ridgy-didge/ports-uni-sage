<?php // get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>

<?php endif; ?>

<?php 
if ( have_posts() ) { ?>

	<div id="<?php the_title(); ?>" class="col-12">


		<?php $current_category = single_cat_title("", false); ?>
		<div id="latest" class="row">
			<h3 class="col area_title">
				Category: <?php  echo $current_category; ?>
			</h3>
		</div>
		
		<div class="row">

			<?php 
			$postID = get_cat_ID( $current_category );
			$args = ( array (
				'posts_per_page' => 5,
				'cat' => $postID,
				)
			);
			$query = new WP_Query( $args );
			if ( $query->have_posts() ) { ?>

				<div class="row">
					<h3 class="col sub_area_title">
						Latest <?php  echo $current_category; ?> Articles:
					</h3>
				</div>

				<div id="" class="owl-carousel-archive">
				
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>

					<article class="items d-flex align-middle items flex-column" <?php post_class(); ?> >
					
						<header>
							<?php if ( has_post_thumbnail() ) {?>
								<div class="thumbnail p-1 item">
									<?php the_post_thumbnail(); ?>
								</div>
							<?php } else {?>
								<div class="thumbnail p-1 item">
									<img class="" src="<?php echo get_template_directory_uri(); ?>/dist/images/foot-blue.svg"/>
								</div>
							<?php } ?>
							<div class="card-header-text p-1">
								<div class="card-title">
									<h5><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>
								</div>
								<div class="card-date">
									<p><span>Date created: </span> <time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time></p>
								</div>
								<div class="card-author">
									<p><span>By</span> </p>
									<?php // for each autor required ?>
									<?php $authors = new CoAuthorsIterator(); ?>
									<?php while( $authors->iterate() ) : ?>
										<a class="fn btn-secondary" href="<?= get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author" > <?= get_the_author(); ?> </a>
									<?php endwhile; ?>														
								</div>
								<div class="card-id">
									<p><span>Article ID:</span> <?php echo  get_the_ID() ?></p>
								</div>
							</div>
						</header>

						<div class="entry-summary card-text">
							<?php echo wp_trim_words( get_the_content(), 40, '...' ); ?>
						</div>
						<a class="btn btn-primary read-more justify-content-center mt-auto p-2" href="<?php the_permalink(); ?>">Read More</a>
					</article>
				

				<?php endwhile; ?>
				</div>

			<?php } else { ?>
				<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php } ?>
		
		</div>
		
		<div class="row">

			<?php 
			$articleArray = [];
			?>

			<?php while (have_posts()) : the_post(); // loop articles to find each post type ?>

				<?php 
				
				// Build array
				$postTypeName = get_post_type_object(get_post_type()); // fetch the post type by name
				$postID = get_the_ID();
				$postTitle = get_the_title();

				$articleArray[$postTypeName->labels->slug][$postID] = $postTypeName->labels->name; 
				
	 		    ?>	

			<?php endwhile; ?>

			<?php ksort($articleArray); // sort array into alphabetical order?>
	
			<nav id="side-nav" class="navbar-side navbar collapse navbar-collapse">
				<ul class="nav nav-pills flex-column">
					<li class="nav-item">
						<a class='nav-link col btn btn-secondary' href='#latest'> Latest </a>
					</li>
					<?php $i = 0; ?>
					<?php foreach ($articleArray as $postTypes) { ?>

						<?php
						$section = "section" . $i;
						?>
						<li class="nav-item row">
							<?php echo "<a class='nav-link col btn btn-secondary' href='#" . $section . "'>" . current($postTypes) . "</a>"?>
						</li>
						<?php $i++; ?>
					<?php }	?>
				</ul>
			</nav>

			<?php

			$i = 0;
			foreach ($articleArray as $postTypes) { ?>
				<?php
					$section = "section" . $i;
				?>

  				<div id="<?php echo $section ?>" class="row"> 

  					<div class="row">
						<h3 class="col sub_area_title">
							<?php  echo "Section: " . current($postTypes);  ?>
						</h3>
					</div>			
  					
					<div id="" class="owl-carousel-archive">
						
						<?php foreach ($postTypes as $postType => $id) { ?>
									
							<?php
							$post = get_post( $postType  );
							$required_post = get_post($postType); // getting all information of that post 
							$title = $required_post->post_title; // get the post title 
							$content = $required_post->post_content; //get the post content ?>

							<article class="items d-flex align-middle items flex-column" <?php post_class(); ?> >

								<header>
								  	<?php if ( has_post_thumbnail() ) {?>
									    <div class="thumbnail p-1 item">
									        <?php the_post_thumbnail(); ?>
									    </div>
									<?php } else {?>
									 	<div class="thumbnail p-1 item">
									        <img class="" src="<?php echo get_template_directory_uri(); ?>/dist/images/foot-blue.svg"/>
									    </div>
									<?php } ?>
									<div class="card-header-text p-1">
										<div class="card-title">
											<h5><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>
										</div>
										<div class="card-date">
											<p><span>Date created: </span> <time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time></p>
										</div>
										<div class="card-author">
											<p><span>By</span> </p>
											<?php // for each autor required ?>
											<?php $authors = new CoAuthorsIterator(); ?>
											<?php while( $authors->iterate() ) : ?>
												<a class="fn btn-secondary" href="<?= get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author" > <?= get_the_author(); ?> </a>
											<?php endwhile; ?>														
										</div>
										<div class="card-id">
											<p><span>Article ID:</span> <?php echo  $postType ?></p>
										</div>
									</div>
								</header>

								<div class="entry-summary card-text p-2">
									<?php echo wp_trim_words( get_the_content(), 40, '...' ); ?>

									</div>
									<a class="btn btn-primary read-more justify-content-center mt-auto p-2" href="<?php the_permalink(); ?>">Read More</a>
							</article>
								
						<?php } // END foreach ?>
					</div>
				</div>
			<?php $i++; ?>
			<?php }	?>
		</div>
	</div>
<?php } // end if ?>