<?php // get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

	<div class="row">
		<h1 class="cat-title">Category: <?php single_cat_title(); ?></h1>
		<?php 
		$cats = []; // set empty array to hold each category found

		while (have_posts()) : the_post(); ?>

			<?php
			// SETUP CATEGORY ARRAYS
			$post_categories = wp_get_post_categories( $post->ID ); // get article categories by id
			
			foreach($post_categories as $c){
			    $cat = get_category( $c );
			    // group articles to categories 
			    // POPULATE THE ARRAYS WITH ARTICLE IDS.
	 		    $cats["Categories"][$cat->name][$cat->slug][$post->ID] =  get_the_title($post->ID); 

			} // END SETUP CATEGORY ARRAYS ?>
		<?php endwhile;  ?>

		<?php // create dynamic side nav ?>
		<nav id="side-nav" class="navbar-side navbar collapse navbar-collapse">
			<ul class="nav nav-pills flex-column">

				<?php foreach ($cats["Categories"] as $cat) { ?>
					<?php
					$key_cat = array_search($cat, $cats["Categories"]); // fetch the category title
					?>
					<?php 
					for ($i = 0; $i < count($cat); $i++) { // count objects inside array 	?>
						<li class="nav-item row">
							<img class="align-self-start" src="<?php echo get_template_directory_uri(); ?>/dist/images/foot-left.svg"/>
							<?php echo "<a class='nav-link col' href='#" . key($cat) . "'>" . $key_cat . "</a>"?>
							<img class="align-self-end" src="<?php echo get_template_directory_uri(); ?>/dist/images/foot-right.svg"/>
						</li>
					<?php }	?>
				<?php } // END foreach ?>
			</ul>
		</nav>

		<?php foreach ($cats["Categories"] as $cat) { ?>
			<div id="<?php echo key($cat) ?>" class="col-12">

				<?php 
				$key_cat = array_search($cat, $cats["Categories"]); // fetch the category title
				?>

				<div class="row">
					<h3 class="col sub_area_title">
						<?php echo $key_cat; ?>
					</h3>
				</div>
				<div class="row">
					

					<div id="" class="owl-carousel-archive col-9">

						<?php foreach ($cat as $article_cat) { ?>

							<?php foreach ($article_cat as $article) { ?>

								<?php
								$key = array_search($article, $article_cat); // find article id;
								?>

								<?php
								$post = get_post( $key ); ?>

								<article class="items d-flex align-middle items flex-column" <?php post_class(); ?> >

								<?php 
									$required_post = get_post($key); // getting all information of that post 
									$title = $required_post->post_title; // get the post title 
									$content = $required_post->post_content; //get the post content

									$author_id = $required_post->post_author; 
								?>
									<header>
									  	<?php if ( has_post_thumbnail() ) {?>
										    <div class="thumbnail p-2">
										        <?php the_post_thumbnail(); ?>
										    </div>
										<?php } else {?>
										 	<div class="thumbnail p-2">
										        <img class="" src="<?php echo get_template_directory_uri(); ?>/dist/images/foot-left.svg"/>
										    </div>
										<?php } ?>
										<div class="card-title p-2">
											<a class="" href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
									   	 	<p>Article Created: <time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time> </p>
											<p>By <?php the_author_meta( 'display_name' , $author_id ); ?> </p>
											<p><?php echo "Article ID: " . $key ?></p>
										</div>
									</header>

									<div class="entry-summary card-text p-2">
									    <?php echo wp_trim_words( $content, 50, $more_text ); ?>
									</div>
									<a class="btn btn-primary read-more justify-content-center mt-auto p-2" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</article> 

							<?php } // END foreach ?>	
						<?php } // END foreach ?>
					</div>
				</div>
			</div>
		<?php } // END foreach ?>
	</div>
<?php the_posts_navigation(); ?>
