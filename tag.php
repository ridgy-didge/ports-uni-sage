<?php // get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>


    <?php $current_category = single_cat_title("", false); ?>
    <div id="latest" class="row">
        <h3 class="col area_title">
            Tag: <?php  echo $current_category; ?>
        </h3>
    </div>
    
    <div class="row">

        <?php 
        $tag = get_queried_object(); 
        $args = ( array (
            'posts_per_page' => 5,
            'tag' =>   $tag->slug
            )
        );
        $query = new WP_Query( $args );
        if ( $query->have_posts() ) { ?>

            <div class="row">
                <h3 class="col sub_area_title">
                    Latest <?php  echo $current_category; ?> Articles:
                </h3>
            </div>

            <div id="" class="owl-carousel-archive">
            
            <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                <article class="items d-flex align-middle items flex-column" <?php post_class(); ?> >
                
                    <header>
                        <?php if ( has_post_thumbnail() ) {?>
                            <div class="thumbnail p-1 item">
                                <?php the_post_thumbnail(); ?>
                            </div>
                        <?php } else {?>
                            <div class="thumbnail p-1 item">
                                <img class="" src="<?php echo get_template_directory_uri(); ?>/dist/images/foot-blue.svg"/>
                            </div>
                        <?php } ?>
                        <div class="card-header-text p-1">
                            <div class="card-title">
                                <h5><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>
                            </div>
                            <div class="card-date">
                                <p><span>Date created: </span> <time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time></p>
                            </div>
                            <div class="card-author">
                                <p><span>By</span> </p>
                                <?php // for each autor required ?>
                                <?php $authors = new CoAuthorsIterator(); ?>
                                <?php while( $authors->iterate() ) : ?>
                                    <a class="fn btn-secondary" href="<?= get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author" > <?= get_the_author(); ?> </a>
                                <?php endwhile; ?>														
                            </div>
                            <div class="card-id">
                                <p><span>Article ID:</span> <?php echo  get_the_ID() ?></p>
                            </div>
                        </div>
                    </header>

                    <div class="entry-summary card-text">
                        <?php echo wp_trim_words( get_the_content(), 40, '...' ); ?>
                    </div>
                    <a class="btn btn-primary read-more justify-content-center mt-auto p-2" href="<?php the_permalink(); ?>">Read More</a>
                </article>
            

            <?php endwhile; ?>
            </div>

        <?php } else { ?>
            <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
        <?php } ?>
    
    </div>

    <div class="row">
       <h1 class="cat-title">Articles with tag: <?php single_tag_title(); ?></h1>
        <?php 
        $cats = []; // set empty array to hold each category found

        while (have_posts()) : the_post(); ?>

            <?php
            // SETUP CATEGORY ARRAYS
            $post_categories = wp_get_post_categories( $post->ID ); // get article categories by id
            
            foreach($post_categories as $c){
                $cat = get_category( $c );
                // group articles to categories 
                // POPULATE THE ARRAYS WITH ARTICLE IDS.
                $cats["Categories"][$cat->name][$cat->slug][$post->ID] =  get_the_title($post->ID); 

            } // END SETUP CATEGORY ARRAYS ?>
        <?php endwhile;  ?>

        <?php // create dynamic side nav ?>
        <nav id="side-nav" class="navbar-side navbar collapse navbar-collapse">
            <ul class="nav nav-pills flex-column">
                <li class="nav-item">
						<a class='nav-link col btn btn-secondary' href='#latest'> Latest </a>
					</li>

                <?php foreach ($cats["Categories"] as $cat) { ?>
                    <?php
                    $key_cat = array_search($cat, $cats["Categories"]); // fetch the category title
                    ?>
                    <?php 
                    for ($i = 0; $i < count($cat); $i++) { // count objects inside array    ?>
                        <li class="nav-item">
                            <?php echo "<a class='nav-link col btn btn-secondary' href='#" . key($cat) . "'>" . $key_cat . "</a>"?>
                        </li>
                    <?php } ?>
                <?php } // END foreach ?>
            </ul>
        </nav>

        <?php foreach ($cats["Categories"] as $cat) { ?>
            <div id="<?php echo key($cat) ?>" class="col-12">

              

                <?php 
                $key_cat = array_search($cat, $cats["Categories"]); // fetch the category title
                ?>

                <div class="row">
                    <h3 class="col sub_area_title">
                        Category: <?php echo $key_cat; ?>
                    </h3>
                </div>
                <div class="row">
                    <?php foreach ($cat as $article_cat) { ?>
                    <?php } // END foreach ?>

                    <div id="" class="owl-carousel-archive">

                        <?php foreach ($cat as $article_cat) { ?>

                            <?php foreach ($article_cat as $article) { ?>

                                <?php
                                $key = array_search($article, $article_cat); // find article id;

                                $required_post = get_post($key); // getting all information of that post 
                                $title = $required_post->post_title; // get the post title 
                                $content = $required_post->post_content; //get the post content
                                ?>

                                <?php
                                $post = get_post( $key ); ?>

                                <article class="items d-flex align-middle items flex-column" <?php post_class(); ?> >

                                    
                                    <header>
                                        <?php if ( has_post_thumbnail() ) {?>
                                            <div class="thumbnail p-2 item">
                                                <?php the_post_thumbnail(); ?>
                                            </div>
                                        <?php } else {?>
                                            <div class="thumbnail p-2 item">
                                                <img class="" src="<?php echo get_template_directory_uri(); ?>/dist/images/foot-blue.svg"/>
                                            </div>
                                        <?php } ?>
                                        <div class="card-title p-2">
                                            <h5><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>
                                            <p>Date Created: <?php get_template_part('templates/entry-meta'); ?> </p>
                                            <p><?php echo "Article ID: " . $key ?></p>
                                    </header>

                                    <div class="entry-summary card-text p-2">
                                        <?php echo wp_trim_words( $content, 30 ); ?>
                                    </div>
                                    <a class="btn btn-primary read-more justify-content-center mt-auto p-2" href="<?php the_permalink(); ?>">Read More</a>
                                </article> 


                            <?php } // END foreach ?>   
                        <?php } // END foreach ?>
                    </div>
                </div>
            </div>
        <?php } // END foreach ?>
    </div>
<?php the_posts_navigation(); ?>
