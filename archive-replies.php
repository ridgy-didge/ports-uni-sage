<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

	<div class="row">
		
		<?php 
		$cats = []; // set empty array to hold each category found

		while (have_posts()) : the_post(); ?>

			<?php
			// SETUP CATEGORY ARRAYS
			$post_categories = wp_get_post_categories( $post->ID ); // get article categories by id

			$taxonomy_tags = 'replyCategories';
          	$tags_list = wp_get_post_terms($post->ID, $taxonomy_tags, array("fields" => "all"));
			
			foreach($tags_list  as $c){
			    $cat = get_category( $c );
			    // group articles to categories 
			    // POPULATE THE ARRAYS WITH ARTICLE IDS.
	 		    $cats["Categories"][$cat->name][$cat->slug][$post->ID] =  get_the_title($post->ID); 

			} // END SETUP CATEGORY ARRAYS 

			?>
		<?php endwhile;  ?>

		<?php // create dynamic side nav ?>
		<nav id="side-nav" class="navbar-side navbar collapse navbar-collapse">
			<ul class="nav nav-pills flex-column">

				<?php foreach ($cats["Categories"] as $cat) { ?>
					<?php
					$key_cat = array_search($cat, $cats["Categories"]); // fetch the category title
					?>
					<?php 
					for ($i = 0; $i < count($cat); $i++) { // count objects inside array 	?>
						<li class="nav-item row">
							<img class="align-self-start" src="<?php echo get_template_directory_uri(); ?>/dist/images/foot-left.svg"/>
							<?php echo "<a class='nav-link col' href='#" . key($cat) . "'>" . $key_cat . "</a>"?>
							<img class="align-self-end" src="<?php echo get_template_directory_uri(); ?>/dist/images/foot-right.svg"/>
						</li>
					<?php }	?>
				<?php } // END foreach ?>
			</ul>
		</nav>

		<?php foreach ($cats["Categories"] as $cat) { ?>
			<div id="<?php echo key($cat) ?>" class="col-12">

				<?php 
				$key_cat = array_search($cat, $cats["Categories"]); // fetch the category title
				?>

				<div class="row">
					<div class="col-12">
						<h3 class="area-title">
							<?php echo "Sub Category: " . $key_cat; ?>
						</h3>
					</div>
				</div>

				<?php foreach ($cat as $article_cat) { ?>
					<div id="articleCountHolder"class="col-12">
						<?php // count articles inside each category
						$art_count = count($article_cat);
							echo "<p>Article Count: <span class='post-count'>" . $art_count . "</span></p>";
						?>
					</div>
				<?php } // END foreach ?>

				<div id="" class="owl-carousel-archive">

					<?php foreach ($cat as $article_cat) { ?>

						<?php foreach ($article_cat as $article) { ?>

							<?php
							$key = array_search($article, $article_cat); // find article id;
							?>

							<?php
							$post = get_post( $key ); ?>

							<article class="justify-content-center items" <?php post_class(); ?> >

								
								<header>
								  	<?php if ( has_post_thumbnail() ) {?>
									    <div class="thumbnail">
									        <?php the_post_thumbnail(); ?>
									    </div>
									<?php } else {?>
									 	<div class="thumbnail">
									        <img class="" src="<?php echo get_template_directory_uri(); ?>/dist/images/foot-left.svg"/>
									    </div>
									<?php } ?>
									<div class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								    <?php get_template_part('templates/entry-meta'); ?>
									<?php echo "Article ID: " . $key ?>
								</header>

								<div class="entry-summary card-text">
								    <?php the_excerpt(); ?>
								</div>
							</article> 

						<?php } // END foreach ?>	
					<?php } // END foreach ?>
				</div>
			</div>
		<?php } // END foreach ?>
	</div>
<?php the_posts_navigation(); ?>
