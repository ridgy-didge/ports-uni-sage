<?php // get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>

<?php endif; ?>

<?php 
if ( have_posts() ) { ?>

	<div id="<?php the_title(); ?>" class="col-12">

		<?php $current_category = single_cat_title("", false); ?>
		<div class="row">
			<h3 class="col area_title">
				Category: <?php  echo $current_category; ?>
			</h3>
		</div>
		
		<div class="row">

			<?php 
			$articleArray = [];
			?>

			<?php while (have_posts()) : the_post(); // loop articles to find each post type ?>

				<?php 
				
				// Build array
				$postTypeName = get_post_type_object(get_post_type()); // fetch the post type by name
				$postID = get_the_ID();
				$postTitle = get_the_title();

				$articleArray[$postTypeName->labels->slug][$postID] = $postTypeName->labels->name; 
				
	 		    ?>	

			<?php endwhile; ?>


			<nav id="side-nav" class="navbar-side navbar collapse navbar-collapse">
				<ul class="nav nav-pills flex-column">
					<?php $i = 0; ?>
					<?php foreach ($articleArray as $postTypes) { ?>

						<?php
						$section = "section" . $i;
						?>
						<li class="nav-item row">
							<img class="align-self-start" src="<?php echo get_template_directory_uri(); ?>/dist/images/foot-left.svg"/>
							<?php echo "<a class='nav-link col' href='#" . $section . "'>" . current($postTypes) . "</a>"?>
							<img class="align-self-end" src="<?php echo get_template_directory_uri(); ?>/dist/images/foot-right.svg"/>
						</li>
						<?php $i++; ?>
					<?php }	?>
				</ul>
			</nav>

		
			<?php
			$i = 0;
			foreach ($articleArray as $postTypes) { ?>
				<?php
					$section = "section" . $i;
				?>

  				<div id="<?php echo $section ?>" class="row"> 

  					<div class="row">
						<h3 class="col sub_area_title">
							<?php  echo current($postTypes);  ?>
						</h3>
					</div>
											
  					<div id="" class="owl-carousel-archive col-9">

						
					    <?php foreach ($postTypes as $id => $postType) { // loop article ids per post type inside array ?>
					    	<article class="justify-content-center items" <?php post_class(); ?> >

					    		<?php 
					    		$content_post = get_post($id);
						     	$content = $content_post->post_content;
						     	$content = apply_filters('the_content', $content);
								$content = str_replace(']]>', ']]&gt;', $content);
								$thumbnail = get_the_post_thumbnail($id);
								$postTitle = get_the_title($id);
								$postDate = get_the_date();
								?>

					    		<header>
				    				<?php if ( has_post_thumbnail($content_post) ) {?>
									    <div class="thumbnail">
									        <?php echo $thumbnail;; ?>
									    </div>
									<?php } else {?>
									 	<div class="thumbnail">
									        <img class="" src="<?php echo get_template_directory_uri(); ?>/dist/images/foot-left.svg"/>
									    </div>
									<?php } ?>
					    		</header>
					    		<div class="card-title">
									<a class="" href="<?php the_permalink($id); ?>"><h3><?php echo $postTitle ?></h3></a>
							   	 	<p>Date Created: 
								   	 	<time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
										<p class="byline author vcard"><?= __('By', 'sage'); ?> <a href="<?= get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author" class="fn"><?= get_the_author(); ?></a></p>
									</p>
									<p><?php echo "Article ID: " . $id ?></p>
								</div>
								<div class="entry-summary card-text">
								    <?php the_excerpt($id); ?>
								</div>

							</article>
					    <?php } ?>
					</div>
				</div>
			<?php $i++; ?>
			<?php }	?>
		</div>
	</div>
<?php } // end if ?>