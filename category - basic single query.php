<?php // get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>

<?php endif; ?>

<?php 
if ( have_posts() ) { ?>

	<div id="<?php the_title(); ?>" class="col-12">

		<?php $current_category = single_cat_title("", false); ?>

		<div class="row">
			<h3 class="col area_title">
				Category: <?php  echo $current_category; ?>
			</h3>
		</div>
		<div class="row">

			<div id="" class="owl-carousel-archive col-9">
				<?php while ( have_posts() ) {
				the_post();  ?>
			

					<article class="items d-flex align-middle items flex-column" <?php post_class(); ?> >

					<?php 

						$author_id = $required_post->post_author; 
						$postid = get_the_ID(); 
					?>
						<header>
						  	<?php if ( has_post_thumbnail() ) {?>
							    <div class="thumbnail p-2">
							        <?php the_post_thumbnail(); ?>
							    </div>
							<?php } else {?>
							 	<div class="thumbnail p-2">
							        <img class="" src="<?php echo get_template_directory_uri(); ?>/dist/images/foot-left.svg"/>
							    </div>
							<?php } ?>
							<div class="card-title p-2">
								<h5><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>
						   	 	<p>Article Created: <time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time> </p>
								<p>By <?php the_author_meta( 'display_name' , $author_id ); ?> </p>
								<p><?php echo "Article ID: " . $postid ?></p>

							</div>
						</header>

						<div class="entry-summary card-text p-2">
						    <?php echo wp_trim_words( get_the_content(), 30, $more_text ); ?>

						</div>
						<a class="btn btn-primary read-more justify-content-center mt-auto p-2" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</article> 
					
				<?php } // end while ?>
			</div>
		</div>
	</div>
	
<?php } // end if ?>