<?php
/**
 * Template Name: Authors List Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
	

	<div id="authors-container" class="row  justify-content-md-center">
		<div id="authors-content" class="col-12  my-auto">
			<?php get_template_part('templates/page', 'header'); ?>
			<?php get_template_part('templates/content', 'page'); ?>
		</div>
	</div>


<?php $allUsers = get_users('orderby=post_count&order=DESC');

$users = array();

// Remove subscribers from the list as they won't write any articles
foreach($allUsers as $currentUser)
{
	if(!in_array( 'subscriber', $currentUser->roles ))
	{
		$users[] = $currentUser;
	}
}

?>
<section class="content" role="main">
	<?php

		foreach($users as $user)
		{
			?>
			<div class="author">
				<div class="authorAvatar">
					<?php echo get_avatar( $user->user_email, '128' ); ?>
				</div>
				<div class="authorInfo">
					<h2 class="authorName"><?php echo $user->display_name; ?></h2>
					<p class="authorDescrption"><?php echo get_user_meta($user->ID, 'description', true); ?></p>
					<p class="authorLinks"><a href="<?php echo get_author_posts_url( $user->ID ); ?>">View Author Links</a></p>
					
				</div>
			</div>
			<?php
		}
	?>
</section>




<?php endwhile; ?>
