

   
    <?php while ( have_posts() ) : the_post(); ?>
          <h1 class="entry-title"><?php the_title(); ?></h1>
          <?php /* echo get_avatar( get_the_author_email(), '128', '/images/no_images.jpg', get_the_author() ); */
          if ( function_exists( 'get_coauthors' ) ) {
            $coauthors = get_coauthors(); ?>
            <h4>Authors</h4>  
            <div class="row">
              <?php
              foreach ( $coauthors as $coauthor ) { ?>
                
                  <div id="authors-area" class="col-4 col-md-2 col-lg-3">
                    <?php $archive_link = get_author_posts_url( $coauthor->ID, $coauthor->user_nicename );
                    $link_title = 'Posts by ' . $coauthor->display_name;
                    ?>
                    <?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>
                   
                    <br>
                     <a class="author-link btn btn-secondary" href="<?php echo esc_url( $archive_link ); ?>" title="<?php echo esc_attr( $link_title ); ?>"><?php echo $coauthor->display_name; ?></a>

                </div>
            <?php  }
            // treat author output normally
            } else {
              $archive_link = get_author_posts_url( get_the_author_meta( 'ID' ) );
              $link_title = 'Posts by ' . the_author();

              ?>
                <a class="author-link" href="<?php echo esc_url( $archive_link ); ?>" title="<?php echo esc_attr( $link_title ); ?>"><?php echo get_avatar( get_the_author_meta( 'user_email' ), 65 ); ?></a>
            <?php } ?>

            <?php
            $post_tags = get_the_tags($tag->term_id); // get all current post tags
            $categories = get_the_category(); // get posts category name
            $category_id = get_cat_ID( $categories[0]->name); // Get the ID of a given parent category
            $category_link = get_category_link( $category_id ); // Get the URL of this category
            $post_type = get_post_type(); // get all post types to include in query
            $postType = get_post_type_object(get_post_type()); // get post type object
            $post_id = get_the_id(); // get post id to exclude ('post__not_in') from list
            $post_type_name = esc_html($postType->labels->name);  // custom post name
            ?>

            <div class="col">
              <div  class="col-12">
                <p id="post-created"><b>Article Created: </b> <?php echo get_the_date(); ?></br>
                <b>Last Modified: </b><?php the_modified_date(); ?></br>
                <b>Article ID: </b><?php echo $post_id ?></p>
              </div>
            </div>
          </div>
        
       
        
           

          <div class="row">
            <!-- Build and display a link to this parent category -->

            <div class="col">
              <h3 class="sub_area_title">Subject Category:</h3>
              <?php
              // list tags and tag posts
              if ( $categories ) {
                foreach( $categories as $cat ) { 
                  // Get the ID of a given category
                  $category_id = get_cat_ID( $cat->name );

                  // Get the URL of this category
                  $category_link = get_category_link( $category_id ); ?>
                  <a class="area_title btn btn-secondary" href="<?php echo esc_url( $category_link ); ?>" title="<?php echo esc_html( $cat->name ); ?>
                  "><?php echo esc_html( $cat->name ); ?>
                  </a>
                <?php }
              } else {
                   echo "<p>Article has no Category</p>";
              } ?>
            </div>


            <div class="col">
              <h3 class="sub_area_title">Topic:</h3>
              <a class="area_title btn btn-secondary" href="<?php echo get_post_type_archive_link( $post_type ); ?>"><?php echo $post_type_name ?></a> 
            </div>
            <div class="col">
              <h3 class="sub_area_title">Tags:</h3>
              <?php
              // list tags and tag posts
              if ( $post_tags ) {
                foreach( $post_tags as $tag ) { ?>
                  <a class="area_title btn btn-secondary" href="<?php echo get_tag_link($tag->term_id) ?>"><?php echo $tag->name ?></a>
                <?php }
              } else {
                   echo "<p>Article has no Tags</p>";
              } ?>
            </div>
          </div>
      

          <div class="entry-content">
           
            <?php the_content(); ?>
          </div>
          <footer class="col">

            <?php
            $defaults = array(
              'before'           => '<nav class="page-nav">' . __( 'Article Pages: ' ),
              'after'            => '</nav>',
              'link_before'      => '',
              'link_after'       => '',
              'next_or_number'   => 'number',
              'separator'        => ' ',
              'nextpagelink'     => __( 'Next page' ),
              'previouspagelink' => __( 'Previous page' ),
              'pagelink'         => '%',
              'echo'             => 1
            );
            wp_link_pages( $defaults );
            ?>
            <?php // wp_link_pages(['before' => '<nav class="page-nav "><p class="page-link">' . __('', 'sage'), 'after' => '</p></nav>']); ?>
          </footer>

          <?php 
          if( have_rows('article_references') ): ?>

            <div id="articleReference" class="">
              <div id="tagTitleHolder" class="">
                <h4 id="tagTitle" class="area-title references">Article References</h4>
              </div>
              
              <div id="arrowToggleHolder"  class="">
                <p id="arrow" class="fa fa-arrow-circle-down" data-toggle="collapse" href="#collapseReference" aria-expanded="false" aria-controls="collapseReference"></p>
              </div>
            </div>
            <div class="collapse" id="collapseReference">
              <div class="reference-description">
                <?php // loop through the rows of data
                while ( have_rows('article_references') ) : the_row(); ?>

                    <h3 class="entry-title">
                      <?php
                      the_sub_field('article_reference_title');
                      ?>
                    </h3>

                    <?php
                    the_sub_field('article_reference_description');
                    ?>
                
                    <?php // display a sub field value
                    
                endwhile; ?>
              </div> 
            </div> 

          <?php else :

            // no rows found

          endif;
          ?>           



          <div id="relatedArticles"  class="row justify-content-around">
            <?php
            /*
            *  Query posts for a relationship value.
            *  This method uses the meta_query LIKE to match the string "123" to the database value a:1:{i:0;s:3:"123";} (serialized array)
            */ ?>
            <div class="col">
              <?php
              echo "<h5 class='sub_area_title col-12'>Article Responses To " . esc_html( get_the_title() ) . "</h5>"; $replies = get_posts(array(
                'post_type' => $post_type,
                'meta_query' => array(
                  array(
                    'key' => 'response_to', // name of custom field
                    'value' => '"' . get_the_ID() . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
                    'compare' => 'LIKE'
                  )
                )
              ));

              $art_count = count($replies);
              $i = 0;
              if( $art_count >  0) { ?>

                  
                  <?php foreach( $replies as $reply ): 
                    $i++;  ?>
                    <div class="relative card col-12">
                      <div id="bgThumbnail" class="absolute">
                       <?php echo get_the_post_thumbnail( $reply->ID, 'large' ); ?>
                      </div>
                      <div class="row header-holder">
                        <div id="tagTitleHolder" class="col-12 col-md-10">
                          <h4 id="tagTitle" ><?php echo get_the_title( $reply->ID ); ?></h4>
                        </div>
                        <div id="arrowToggleHolder"  class="col-12 col-md-2">
                          <p id="arrow" class="fa fa-arrow-circle-down" data-toggle="collapse" href="#hiddenContent<?php echo $i ?>" aria-expanded="false" aria-controls="collapseExample"></p>
                        </div>
                      </div>
                      <div id="hiddenContent<?php echo $i ?>" class="collapse">
                        <p class="col-12"><?php echo wp_trim_words( get_the_content($reply->ID), 40, '...' ); ?></p>
                      </div>
                      <div class="row justify-content-md-center">
                        <a class="btn btn-primary read-more  col-12 col-md-8 col-lg-6" href="<?php echo get_permalink( $reply->ID ); ?>">
                          Read More
                        </a>
                      </div>
                      <?php wp_reset_postdata(); // Restore original Post Data ?>
                      
                    </div>
                  <?php endforeach; ?>

              <?php   } else {
                 // no posts found
                echo "<div class='col not-found'><h3> No replies found </h3></div>";
                wp_reset_postdata(); // Restore original Post Data
              };  ?>
            </div>

            <div class="col">
              <?php  echo "<h5 class='sub_area_title col-12'>Replies To " . esc_html( get_the_title() ) . "</h5>";

              $replies = get_posts(array(
                  'post_type' => 'replies',
                  'meta_query' => array(
                    array(
                      'key' => 'response_to', // name of custom field
                      'value' => '"' . get_the_ID() . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
                      'compare' => 'LIKE'
                    )
                  )
                ));
                $art_count = count($replies);
                if( $art_count >  0) { ?>
               
                    
                    <?php foreach( $replies as $reply ):
                      $i++;  ?>
                      <div class="relative card col-12">
                        <div id="bgThumbnail" class="absolute">
                          <?php echo get_the_post_thumbnail( $reply->ID, 'large' ); ?>
                        </div>
                        <div class="row">
                          <div id="tagTitleHolder" class="col-12 col-md-10">
                            <h4 id="tagTitle"><?php echo get_the_title( $reply->ID ); ?></h4>
                          </div>
                          <div id="arrowToggleHolder"  class="col-12 col-md-2">
                            <p id="arrow" class="fa fa-arrow-circle-down" data-toggle="collapse" href="#hiddenContent<?php echo $i ?>" aria-expanded="false" aria-controls="collapseExample"></p>
                          </div>
                        </div>
                        <div id="hiddenContent<?php echo $i ?>" class="collapse">
                          <P class="col-12">
                            <?php echo wp_trim_words( get_the_content($reply->ID), 40, '...' ); ?>
                          </p>
                        </div>
                        <div class="row justify-content-md-center">
                        <a class="btn btn-primary read-more  col-12 col-md-8 col-lg-6" href="<?php echo get_permalink( $reply->ID ); ?>">
                          Read More
                        </a>
                      </div>
                        <?php wp_reset_postdata(); // Restore original Post Data ?>
                      </div>
                   <?php endforeach; ?>
                 
                <?php   } else {
                   // no posts found
                  echo "<div class='col not-found'><h3> No replies found </h3></div>";
                  
                };  ?>
              
          </div>



          <div class="row">
            <div id="relatedArticles"  class="col-12 col-lg-6">

              <h5 class="sub_area_title"> Latest related <?php echo  $post_type_name ?> articles</h5>
              <?php
              $args = array(
                'posts_per_page' => '3',
                'post_type' => $post_type, // include only current post type
                'post__not_in' => array($post_id), // do not include this current article
              );
              $the_query = new WP_Query( $args );
              // loop posts
              if ( $the_query->have_posts() ) {
                while ( $the_query->have_posts() ) {
                        $the_query->the_post();
                  $i++;  ?>
                  <div class="relative card col-12">
                    <div id="bgThumbnail" class="absolute">
                      <?php echo get_the_post_thumbnail( $page->ID, 'large' ); ?>
                    </div>
                    <div class="row">
                      <div id="tagTitleHolder" class="col-12 col-md-10">
                        <h4 id="tagTitle" class="" ><?php the_title(); ?></h4>
                      </div>
                       <div id="arrowToggleHolder"  class="col-12 col-md-2">
                        <div id="arrow" class="fa fa-arrow-circle-down" data-toggle="collapse" href="#hiddenContent<?php echo $i ?>" aria-expanded="false" aria-controls="collapseExample"></div>
                      </div>
                    </div>
                    <div id="hiddenContent<?php echo $i ?>" class="collapse">
                      <P class="col-12">
                        <?php echo wp_trim_words( get_the_content(), 40, '...' ); ?>
                      </p>
                    </div>
                   
                    <div class="row justify-content-md-center">
                      <a class="btn btn-primary read-more  col-12 col-md-8 col-lg-6" href="<?php echo get_permalink( ); ?>">
                        Read More
                      </a>
                    </div>
                    <?php wp_reset_postdata(); // Restore original Post Data ?>
                  </div>
                <?php }
                 
              } else {
                 // no posts found
                  echo "<p class='not-found'>No other matching articles found</p>";
                  wp_reset_postdata(); // Restore original Post Data
              }
              ?>
            </div>


            <div id="relatedCategories" class="col-12 col-lg-6">
              <h5 class="sub_area_title"> Latest related <?php echo esc_html( $categories[0]->name ); ?> articles </h5>
              <?php
              $post_types = get_post_types(); // get all post types to include in query
              $post_id = get_the_id(); // get post id to exclude ('post__not_in') from list
              $args = array(
                'posts_per_page' => '3',
                'post_type' => $post_types,  // $post_types finds all post types
                'category__in' => $category_id, // only articles from the same category
                'post__not_in' => array($post_id), // do not include this current article
              );
              $the_query = new WP_Query( $args );

              // loop posts
              if ( $the_query->have_posts() ) {
                while ( $the_query->have_posts() ) {
                        $the_query->the_post();
                         $i++;  ?>
                  <div class="relative card col-12">
                    <div id="bgThumbnail" class="absolute">
                      <?php echo get_the_post_thumbnail( $page->ID, 'large' ); ?>
                    </div>
                   <div class="row">
                      <div id="tagTitleHolder" class="col-12 col-md-10">
                        <h4 id="tagTitle" class="" ><?php the_title(); ?></h4>
                      </div>
                       <div id="arrowToggleHolder"  class="col-12 col-md-2">
                        <div id="arrow" class="fa fa-arrow-circle-down" data-toggle="collapse" href="#hiddenContent<?php echo $i ?>" aria-expanded="false" aria-controls="collapseExample"></div>
                      </div>
                    </div>
                    <div id="hiddenContent<?php echo $i ?>" class="collapse">
                      <P class="col-12">
                        <?php echo wp_trim_words( get_the_content(), 40, '...' ); ?>
                      </p>
                    </div>
                    <div class="row justify-content-md-center">
                      <a class="btn btn-primary read-more  col-12 col-md-8 col-lg-6" href="<?php echo get_permalink( ); ?>">
                        Read More
                      </a>
                    </div>
                    <?php wp_reset_postdata(); // Restore original Post Data ?>
                  </div>
                <?php }
                 
              } else {
                 // no posts found
                  echo "<p class='not-found'>No other matching articles found</p>";
                  wp_reset_postdata(); // Restore original Post Data
              }
              ?>
            </div>
          </div>

          <div id="relatedTags" class="col">
            <h5 class="sub_area_title col-12"> Related Tagged Articles </h5>
            <?php
            $post_tag = get_the_tags($tag->term_id); // get all current post tags

            if ( $post_tags ) {

              // list tags and tag posts
              foreach( $post_tags as $tag ) { ?>
                
                <?php $args = array (
                  'posts_per_page' => '2',
                  'post_type' => $post_types,  // $post_types finds all post types
                  'tag_id' => $tag->term_id,
                  'post__not_in' => array($post_id), // do not include this current article
                );
                $the_query = new WP_Query( $args ); ?>

                <div class="row">
                <?php if ( $the_query->have_posts() ) { ?>
                 <h5 class="sub_area_title col-12">Latest Articles with Tag: <a class="btn btn-secondary" href="<?php echo get_tag_link($tag->term_id) ?>"><?php echo $tag->name ?></a></h5>

                  <?php
                  // query tag posts
                 while ( $the_query->have_posts() ) {
                        $the_query->the_post();
                  $i++;  ?>
                  <div id="tagBox" class="relative col-12 col-md">
                    
                     <div id="bgThumbnail" class="absolute">
                      <?php echo get_the_post_thumbnail( $page->ID, 'large' ); ?>
                    </div>
                    <div id="" class="row">
                      <div id="tagTitleHolder" class="col-12 col-md-9">
                        <h4 id="tagTitle" class="" ><?php the_title(); ?></h4>
                      </div>
                      <div id="arrowToggleHolder"  class="col-12 col-md-3">
                        <div id="arrow" class="fa fa-arrow-circle-down" data-toggle="collapse" href="#hiddenContent<?php echo $i ?>" aria-expanded="false" aria-controls="collapseExample"></div>
                      </div>
                    </div>
                    <div id="hiddenContent<?php echo $i ?>" class="collapse">
                      <P class="col-12">
                        <?php echo wp_trim_words( get_the_content(), 40, '...' ); ?>
                      </p>
                    </div>
                    <div class="row justify-content-md-center">
                      <a class="btn btn-primary read-more  col-12 col-lg-10 col-xl-8" href="<?php echo get_permalink( ); ?>">
                        Read More
                      </a>
                    </div>
                    <?php wp_reset_postdata(); // Restore original Post Data ?>
                  </div>

                <?php }
                
                } else { 
                  // no posts found ?>
                   <h4 class=""> <?php echo "No other matching articles found"; ?> </h4>
                <?php } ?>
               </div> 
              <?php } ?>
             
            <?php } else {
                echo "<p class='not-found'>Article has no Tags</p>";
                wp_reset_postdata(); // Restore original Post Data

            }
          ?>
          </div>
          <div id="bodySidebar" class="row">
            <?php get_template_part('templates/sidebar-body'); ?>
          </div>

<?php endwhile; ?>
