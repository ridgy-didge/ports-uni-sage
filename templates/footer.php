
<footer class="container-fluid content-info footer">

  <div class="container">
    <div class="row">
    	 <div class="logo-container">
              <a class="logo" href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img class="logo" src='<?php echo esc_url( get_theme_mod( 'upload_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
          </div>
    	<div id="footer-sidebar" class="col-12 col-lg-10">
    		<div class="row">
      		<?php dynamic_sidebar('sidebar-footer'); ?>
      	</div>
      </div>
    </div>
    <div class="">
      Copyright Info
    </div>
  </div>
</footer>

