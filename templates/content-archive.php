<article class="card col-12 col-md-6 co-lg-3" <?php post_class(); ?> >
  <header>
  	<p>archive</p>
  	<div class="card-img-top" ><?php the_post_thumbnail(); ?></div>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php get_template_part('templates/entry-meta'); ?>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
</article>
