<header id="header" class="banner">
    <div class="container">
        <div class="header ">
            <div class="row ">
                <div class="logo-container row justify-content-center" >
                    <a class="col-10 col-sm-6 col-md-4 col-lg-2"href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img class="logo" src='<?php echo esc_url( get_theme_mod( 'upload_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>

                     <h2 class='site-description col-12 col-md-8 col-lg-10'><?php bloginfo( 'description' ); ?></h2>
                </div>
                


                 <div class="col-12">
                    

                    <div class="col search_form">
                      
                      
                        <nav class="navbar navbar-default col-12" role="navigation">
                           

                            <?php
                                wp_nav_menu( array(
                                    'menu'              => 'primary_navigation',
                                    'theme_location'    => 'primary_navigation',
                                    'depth'             => 2,
                                    'container'         => 'div',
                                    'container_class'   => 'collapse navbar-collapse ',
                                    'container_id'      => 'bs-navbar-collapse',
                                    'menu_class'        => 'nav navbar-nav',
                                    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                                    'walker'            => new WP_Bootstrap_Navwalker())
                                );
                            ?>
                        </nav>
                    </div>
                   
                    <div id="searchModal" class="modal fade" >
                        <div class="modal-dialog" role="document">
                             <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title p-2">Search Nurture Science</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">Close</button>
                                </div>
                                <div class="modal-body">
                                    <form role="search" method="get" class="search-form col-12 " action="<?php echo home_url( '/' ); ?>">
                                        <label class="row d-flex align-items-stretch">
                                            <span class="screen-reader-text">Search for:</span>
                                            <input type="text" value="" name="s" id="searchBox" placeholder="Search For..." class=""/></input>
                                            <label>in Category: <?php wp_dropdown_categories( 'show_option_all=All Categories' ); ?> (Optional). </label> 
                                            <label>with Tag: <?php wp_dropdown_categories('taxonomy=post_tag'); ?> (Optional). </label> 
                                            <button type="submit" id="searchsubmit" class="search-submit col-4" /></button>
                                        </label>
                                    </form>
                                </div>

                               

                                <div class="modal-footer">
                                    
                                </div>
                            </div>

                        </div>

                    </div>

                    <div id="donateModal" class="modal fade" >
                           <?php 
                                $page_title = get_page_by_title("Donate to this site"); 
                                $post = get_post($page_title); 
                                $post_title = get_the_title($post) ; ?>                           ?>
                                    
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title p-2"><?php echo $post_title ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">Close</button>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <p class="p-2"><?php echo the_excerpt() ?></p>
                                  </div>
                                  <div class="modal-footer">
                                    <a class="btn btn-primary read-more mt-auto p-2"  href="<?php the_permalink(); ?>">Donate Here</a>
                                     <?php wp_reset_query(); ?>
                                  </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    
                </div>
            </div>

        </div>
    </div>
  
</header>

