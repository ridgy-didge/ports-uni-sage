

   
    <?php while ( have_posts() ) : the_post();
          /* echo get_avatar( get_the_author_email(), '128', '/images/no_images.jpg', get_the_author() ); */ ?>


          <h1>Content Replies</h1>

          
          <?php if ( function_exists( 'get_coauthors' ) ) {
            $coauthors = get_coauthors(); ?>
            <h4>Authors</h4>  
            <div class="row">
              <?php
              foreach ( $coauthors as $coauthor ) { ?>
                
                  <div id="authors-area" class="col-6 col-md-4 col-lg-2">
                    <?php $archive_link = get_author_posts_url( $coauthor->ID, $coauthor->user_nicename );
                    $link_title = 'Posts by ' . $coauthor->display_name;
                    ?>
                    <?php echo get_avatar( get_the_author_meta( 'user_email' ), 65 ); ?>
                   
                    <br>
                     <a class="author-link btn btn-secondary" href="<?php echo esc_url( $archive_link ); ?>" title="<?php echo esc_attr( $link_title ); ?>"><?php echo $coauthor->display_name; ?></a>

                </div>
            <?php  }
            // treat author output normally
            } else {
              $archive_link = get_author_posts_url( get_the_author_meta( 'ID' ) );
              $link_title = 'Posts by ' . the_author();

              ?>
                <a class="author-link" href="<?php echo esc_url( $archive_link ); ?>" title="<?php echo esc_attr( $link_title ); ?>"><?php echo get_avatar( get_the_author_meta( 'user_email' ), 65 ); ?></a>
              
              <?php
            }
            ?>
            <div class="col">
              <div  class="col-12">
                <p id="post-created"><b>Article Created: </b> <?php echo get_the_date(); ?></br>
                <b>Last Modified: </b><?php the_modified_date(); ?></p>
              </div>
            </div>
          </div>

         

         <?php $taxonomy = 'replyCategories';
          $term_list = wp_get_post_terms($post->ID, $taxonomy, array("fields" => "all"));
          $taxonomy_tags = 'responseTags';
          $tags_list = wp_get_post_terms($post->ID, $taxonomy_tags, array("fields" => "all"));
          ?>

          <?php
          $post_type = get_post_type(); // get all post types to include in query
          $postType = get_post_type_object(get_post_type()); // get post type object
          $post_id = get_the_id(); // get post id to exclude ('post__not_in') from list
          $post_type_name = esc_html($postType->labels->name);  // custom post name
          ?>

          <?php
          $terms = get_field('Taxonomy_field');
          echo print_r($terms);
          ?>

          <h1><?php the_field('Taxonomy_field', $post_id ); ?></h1>

          <div class="row">
            <!-- Build and display a link to this parent category -->
            <div class="col">
              <h3 class="area-title">Reply Category:</h3>
              <?php
              // list tags and tag posts
              if ( $term_list ) {
                foreach( $term_list as $cat ) { ?>
                  <?php 
                  $category_id = get_cat_ID($cat->term_id); // Get the ID of a given parent category
                  $category_link = get_category_link( $cat->term_id); // Get the URL of this category
                  ?>
                  <a class="area_title btn btn-secondary" href="<?php echo $category_link; ?>" title="<?php echo esc_html( $cat->name ); ?>
                  ">
                  <?php echo esc_html( $cat->name ); ?>
                  </a>
                <?php }
              } else {
                   echo "<p>Article has no Category</p>";
              } ?>
            </div>
            <div class="col">
              <h3 class="area-title">Topic:</h3>
              <a class="area_title btn btn-secondary" href="<?php echo get_post_type_archive_link( $post_type ); ?>"><?php echo $post_type_name ?></a> 
            </div>
            <div class="col">
              <?php if ( $tags_list ) {
                foreach( $tags_list as $tag ) {
                  // fetch tag slug and str_replace "Article", ""
                  // match against post title slug
                  $article_slug = substr($tag->slug, 0, -8) . " ";
                  $article_name = substr($tag->name, 0, -8) . " ";

                  echo "<h3 class='area-title'>In Response to: " . $article_name . "</h3>";
                  // End find and edit tag name ready to match against post name

                  // Start match updated tag slug to article
                  $args_tags = array(
                    'name' => $article_name,
                    'numberposts' => 3,
                    'category' => 0,
                    'orderby' => 'post_date',
                    'order' => 'DESC',
                    'post_type' => array( 'Pregnancy (0 years)', 'Newborns (0-3mths)', 'Baby (3mths-18mths)', 'Toddlers (18mths-3yrs)', 'Children (3yrs-13yrs)', 'Teens (13yrs-18yrs)', 'Adults (22yrs-65yrs)', 'Elderly (65yrs-above)') ,
                    'post_status' => 'publish',
                    'suppress_filters' => true
                  );

                  $the_query = new WP_Query( $args_tags );

                  if ( $the_query->have_posts() ) { ?>
                   
                    <?php while ( $the_query->have_posts() ) { ?>
                      <?php $the_query->the_post(); ?>
                      <a class="area_title btn btn-secondary" href="<?php echo esc_url( get_permalink($post->ID) )?>"><?php the_title(); ?></a>
                      <?php wp_reset_postdata(); // Restore original Post Data 
                    }
                  } else {
                     // no posts found
                    echo "<p>No other matching articles found</p>";
                  } 
                }
              } ?>
            
        
            </div>
          </div>

          <div class="entry-content">
            <h1 class="entry-title"><?php the_title(); ?></h1>
            <?php the_content(); ?>
          </div>
          <footer>
            <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
          </footer>

          <?php 
          // donation form for article
          if( get_field('code') ): ?>
            <div class="row">
              <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseDonate" aria-expanded="false" aria-controls="collapseExample">
              Donate to this article here
              </button>
              <div class="collapse col-12" id="collapseDonate">
                <?php
                the_field('code');
                ?>
              </div> 
            </div>
          <?php endif; ?>

          <div class="row">
            <div id="related-articles"  class="col-12 col-lg-6">

              <h5 class="area-title"> Latest <?php echo  $post_type_name ?> Articles</h5>
              <?php
              $args = array(
                'posts_per_page' => '3',
                'post_type' => $post_type, // include only current post type
                'post__not_in' => array($post_id), // do not include this current article
              );


              $the_query = new WP_Query( $args );
              // loop posts
              if ( $the_query->have_posts() ) {
                
                ?>  <?php
                while ( $the_query->have_posts() ) {
                  $the_query->the_post(); ?>
                  <div class="relative card">
                  <h4 class="area-tit"><?php the_title(); ?></h4>
                  <?php echo the_excerpt();
                  wp_reset_postdata(); // Restore original Post Data 
                  echo '</div>';
                }
              } else {
                 // no posts found
                echo "<p>No other matching articles found</p>";
              }
              ?>
            </div>

            <div id="related-categories" class="col-12 col-lg-6">
              <h5 class="area-title"> Latest Reply Sub Category Articles</h5>
              <?php
              $post_id = get_the_id(); // get post id to exclude ('post__not_in') from list
              
              if ( $term_list ) {
                foreach( $term_list as $cat ) { 

                  $category_id = $cat->term_id; // Get the ID of a given parent category
                  $category_name = $cat->name;
                  $category_slug = $cat->slug;


                  $args = array(
                   'tax_query' => array(
                        array(
                            'taxonomy' => $taxonomy,
                            'field' => 'slug',
                            'terms' =>  $category_slug,
                        )
                    ),
                    'post__not_in' => array($post_id),
                  );


                  $the_query = new WP_Query( $args );
                  ?>

                  <?php
                  $category_link = get_category_link( $category_id ); // Get the URL of this category ?>
                  
                  <a class="area_title btn btn-secondary" href="<?php echo $category_link; ?>" title="<?php echo esc_html( $category_name ); ?>">
                    <?php echo esc_html( $category_name ); ?>
                  </a>

                  <?php if ( $the_query->have_posts() ) {

                    while ( $the_query->have_posts() ) {
                      $the_query->the_post(); ?>
                      <div class="relative card">
                        <h4><?php the_title(); ?></h4>
                        <?php echo the_excerpt();
                        wp_reset_postdata(); // Restore original Post Data 
                      echo '</div>';
                    }
                  } else {
                     // no posts found
                      echo "<p>No other matching articles found</p>";
                 } 
                }
              } else {
                   echo "<p>Article has no Category</p>";
              } 
              ?>
            </div>

            <div id="related-tags" class="col-12 col-lg-6">
              <h5 class="area-title"> Latest Related Replies to <?php echo esc_html( $tag->name  ); ?> </h5>
             
              <?php
              // list tags and tag posts
              if ( $tags_list ) {

                foreach( $tags_list as $tag ) { ?>
                  <a class="btn btn-secondary" href="<?php echo get_tag_link($tag->term_id) ?>"><?php echo $tag->name ?></a>
                <?php }

                 $args = array(
                   'tax_query' => array(
                        array(
                            'taxonomy' => $taxonomy_tags,
                            'field' => 'slug',
                            'terms' =>  $tag->slug,
                        )
                    ),
                    'post__not_in' => array($post_id),
                  );

                $the_query = new WP_Query( $args );
                if ( $the_query->have_posts() ) { ?>
                 
                  <?php
                  // query tag posts
                  while ( $the_query->have_posts() ) {
                    $the_query->the_post(); ?>
                    <div class="relative card col-12 col-md-6">
                      <h4><?php the_title(); ?></h4>
                      <?php echo the_excerpt();
                      wp_reset_postdata(); // Restore original Post Data 
                      echo '</div>';
                  }    
                } else {  ?>
                  <h4 class=""> <?php echo "No other matching articles found"; ?> </h4>
                <?php }
                  
              } else {
                     echo "<p>Article has no Tags</p>";
              } ?>
            </div>

            <div id="related-tags" class="col-12 col-lg-6">
               <h5 class="area-title"> Excerpt from <?php echo esc_html( $tag->name  ); ?> </h5>
               <?php $the_query = new WP_Query( $args_tags );

                if ( $the_query->have_posts() ) { ?>
                 
                  <?php while ( $the_query->have_posts() ) { ?>
                    <?php $the_query->the_post(); ?>
                    <a class="btn btn-secondary" href="<?php echo esc_url( get_permalink($post->ID) )?>"><?php the_title(); ?></a>

                    <div class="relative card">
                    <h4 class=""><?php the_title(); ?></h4>
                    <?php echo the_excerpt();
                    wp_reset_postdata(); // Restore original Post Data 
                    echo '</div>';
                  }
                } else {
                   // no posts found
                  echo "<p>No other matching articles found</p>";
                } ?>
            </div>  
<?php endwhile; ?>
