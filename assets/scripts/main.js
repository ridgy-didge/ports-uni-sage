/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */


  (function($) {


  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
          // match height
        // $('.item').matchHeight();
        $('.item').matchHeight({byRow:false});
        /* $(window).resize(function(){
          $('.item').matchHeight();
          $('.footer .widget').matchHeight(); 
        }); */

          $('.owl-carousel-archive').owlCarousel({
          pagination: true,
          navigation: true,
          loop:true,
          margin:10,
          navigationText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
          responsiveRefreshRate : 100,
          responsiveBaseWidth: window,
          responsive: true,
          items : 3,
          itemsDesktop : [1499,2],
          itemsDesktopSmall : [980,1],
          itemsTablet: [768,1],
          itemsTabletSmall: false,
          itemsMobile : [479,1],
          singleItem : false,
        }); 
        
          // Add scrollspy to <body>
        $('body').scrollspy({target: "#side-nav", offset: 500});
        // $('body').scrollspy({target: ".toTop", offset: 0});

        // Add smooth scrolling on all links inside the navbar
        $(".nav-link, #toTop").on('click', function(event) {

          // Make sure this.hash has a value before overriding default behavior
          if (this.hash !== "") {

            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            $('html, body').animate({
              scrollTop: $(hash).offset().top
            }, 800, function(){

            // Add hash (#) to URL when done scrolling (default click behavior)
              window.location.hash = hash;
            });
          } // End if

         
        }); 
         // toTop function
        $(window).scroll(function(){
         if ($(window).scrollTop() > 250) {
          $('#toTop').fadeIn();
         } else {
          $('#toTop').fadeOut();
         }
        });

        function scrolltop()
        {
         $('html, body').animate({scrollTop : 0},500);
        }
        
      }

    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
         $('.owl-carousel-latest').owlCarousel({
          pagination: true,
          navigation: true,
          loop:true,
          margin:10,
          navigationText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
          responsiveRefreshRate : 100,
          responsiveBaseWidth: window,
          responsive: true,
          items : 3,
          itemsDesktop : [1499,2],
          itemsDesktopSmall : [980,1],
          itemsTablet: [768,1],
          itemsTabletSmall: false,
          itemsMobile : [479,1],
          singleItem : false,
        });

        $('.owl-carousel-categories').owlCarousel({
          pagination: true,
          navigation: true,
          loop:true,
          margin:10,
          navigationText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
          responsiveRefreshRate : 100,
          responsiveBaseWidth: window,
          responsive: true,
          items : 2,
          itemsDesktop : [1499,2],
          itemsDesktopSmall : [980,1],
          itemsTablet: [768,1],
          itemsTabletSmall: false,
          itemsMobile : [479,1],
          singleItem : false,
        });

        $('.owl-carousel-articles').owlCarousel({
          pagination: true,
          navigation: true,
          loop:true,
          margin:10,
          navigationText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
          responsiveRefreshRate : 100,
          responsiveBaseWidth: window,
          responsive: true,
          items : 3,
          itemsDesktop : [1499,2],
          itemsDesktopSmall : [980,1],
          itemsTablet: [768,1],
          itemsTabletSmall: false,
          itemsMobile : [479,1],
          singleItem : false,
        });
      

      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.


