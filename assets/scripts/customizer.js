// assets/scripts/customizer.js

(function($) {

  // Primary colour
  wp.customize('primary_colour', function(value) {
    value.bind(function(to) {
      $('head').append('<style>.Primary-bg-c{background-color:'+ to +' !important;}</style>');
      $('head').append('<style>.Primary-c{color:'+ to +' !important;}</style>');
      $('head').append('<style>.Primary-c--hover:hover{color:'+ to +' !important;}</style>');
      $('head').append('<style>.Primary-bo-c{border-color:'+ to +' !important;}</style>');
    });
  });

  // Secondary colour
  wp.customize('secondary_colour', function(value) {
    value.bind(function(to) {
      $('head').append('<style>h2::before{background:'+ to +' !important;}</style>');
      $('head').append('<style>.Breakout-text::before,.Breakout-text::after{color:'+ to +' !important;}</style>');
      $('head').append('<style>.Breakout-text::before,.Breakout-text::after{color:'+ to +' !important;}</style>');
    });
  });

  // Header background colour
  wp.customize('header_background_colour', function(value) {
    value.bind(function(to) {
      $('head').append('<style>.Header{background-color:'+ to +' !important;}</style>');
      $('head').append('<style>#shiftnav-main{background-color:'+ to +' !important;}</style>');
    });
  });

  // Header font colour
  wp.customize('header_font_colour', function(value) {
    value.bind(function(to) {
      $('head').append('<style>.Header a{color:'+ to +' !important;}</style>');
      $('head').append('<style>.shiftnav ul.shiftnav-menu li.menu-item > .shiftnav-target {color:'+ to +' !important;}</style>');
    });
  });

  // Mobile menu hover colour
  wp.customize('mobile_menu_hover_colour', function(value) {
    value.bind(function(to) {
      $('head').append('<style>.shiftnav ul.shiftnav-menu li.menu-item > .shiftnav-target:hover {background-color:'+ to +' !important;}</style>');
    });
  });

  // Footer background colour
  wp.customize('footer_background_colour', function(value) {
    value.bind(function(to) {
      $('head').append('<style>.Footer {background-color:'+ to +' !important;}</style>');
    });
  });

  // Footer font colour
  wp.customize('footer_font_colour', function(value) {
    value.bind(function(to) {
      $('head').append('<style>.Footer {color:'+ to +' !important;}</style>');
      $('head').append('<style>.Footer .icon path {fill:'+ to +' !important;}</style>');
    });
  });

  // Address
  wp.customize('address', function(value) {
    value.bind(function(to) {
      $('.Footer-contactDetails address').html(to);
    });
  });

  // Phone number
  wp.customize('phonenumber_humans', function(value) {
    value.bind(function(to) {
      $('#js-phoneNumber').html(to);
    });
  });

  // Email address
  wp.customize('email_address', function(value) {
    value.bind(function(to) {
      $('#js-emailAddress').html(to);
    });
  });


})(jQuery);
