<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?> >
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <button id="donateToggle" type="button" class="btn btn-primary" data-toggle="modal" data-target="#donateModal">
      <p><span class=""></span> Donate</p> 
    </button>
    <button id="searchToggle" type="button" class="btn btn-primary absolute" data-toggle="modal" data-target="#searchModal">
      <p><span class="fa fa-search"></span> Search</p> 
    </button>
    <a id="toTop" class="toTop fa fa-arrow-circle-up"  href="#header"></a>
    <div class="navbar-header">
      <button id="navButton" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    
    <div class="wrap container" role="document">
      <div class="content row">
        <main id="main" class="main">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php if (Setup\display_sidebar()) : ?>
          <aside class="sidebar">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
       
      </div><!-- /.content -->
    </div><!-- /.wrap -->
     <?php
          do_action('get_footer');
          get_template_part('templates/footer');
          wp_footer();
        ?>
  </body>
</html>
