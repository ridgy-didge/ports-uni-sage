<?php
/**
 * Template Name: Homepage Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<nav id="side-nav" class="navbar-side navbar navbar-fixed-top collapse navbar-collapse">
		 <ul class="nav nav-pills flex-column">
	    	<li class="nav-item"><a class="nav-link btn btn-secondary" href="#main">Welcome</a></li>
	    	<li class="nav-item"><a class="nav-link btn btn-secondary" href="#subject-categories">Subject Categories</a></li>
	    	<li class="nav-item"><a class="nav-link btn btn-secondary" href="#latest-articles">Latest Articles</a></li>
	    	<li class="nav-item"><a class="nav-link btn btn-secondary" href="#participate-in-study">Participate</a></li>
	    	<li class="nav-item"><a class="nav-link btn btn-secondary" href="#social-area">Social</a></li>
	    	<li class="nav-item"><a class="nav-link btn btn-secondary" href="#signUp">Sign Up</a></li>
	    	
		</ul>
	</nav>
	

	<div id="homepage-container" class="row justify-content-center">
		<div id="homepage-content"  class="my-auto row justify-content-between align-items-center">
			<div id="homeBGImg" class="absolute">
				<?php the_post_thumbnail('large') ?>
			</div>
			<div id="homeTitle" class="col-12 col-xl-4">
				<?php get_template_part('templates/page', 'header'); ?>
			</div>
			<div id="homeContent" class="col-12 col-xl-6">
				<?php get_template_part('templates/content', 'page'); ?>
			</div>

		</div>

		
	</div>

	<div id="subject-categories" class="col-12 justify-content-around relative">

		<div class="area-divider col col-md-4">
		</div>
		<div class="cat-title col">
			<h3>Article Categories</h3>
		</div>
		<div class="area-divider col col-md-4">
		</div>
		
		<div id="homepage-nav-holder"  class="col-md-12 ">
			
			<?php
			$categories = get_categories( array(
			    'orderby' => 'name',
			    'order'   => 'ASC',
			    'exclude_slugs' => [ 'authors' ],
			) ); ?>

			<div class="owl-carousel owl-carousel-categories">
				<?php foreach( $categories as $category ) { ?>
					<div class="items d-flex align-middle items flex-column align-items-stretch">
						<a class="carousel-link d-flex flex-column align-items-stretch" href="<?php echo get_category_link( $category->term_id ) ?>">
							<?php 					     
							echo '<h4 class="card-title p-1">' . $category->name . '</h4> ';
							echo '<p class="p-1">' . $category->description  . '</p>';
							echo '<div class="btn mt-auto p-1" href="'. get_category_link( $category->term_id ) . '">Read More</div>';
							?>
						</a>
					</div>
				<?php }  ?> 
			</div>
		</div>

	</div>

	<div id="latest-articles" class="row  justify-content-md-center relative"> 
		<div class="area-divider col col-md-4">
		</div>
		<div class="cat-title col">
			<h3>Latest Articles</h3>
		</div>
		<div class="area-divider col col-md-4">
		</div>

		<div class="col my-auto ">
			<div class="container-fluid latest-blog-posts">
				<div id="" class="owl-carousel owl-carousel-latest ">
					<?php $recent_args = array( 
						'numberposts' => 5,
						'category' => 0,
						'orderby' => 'post_date',
						'order' => 'DESC',
						'post_type' => array( 'Pregnancy (0 years)', 'Newborns (0-3mths)', 'Baby (3mths-18mths)', 'Toddlers (18mths-3yrs)', 'Children (3yrs-13yrs)', 'Teens (13yrs-18yrs)', 'Adults (22yrs-65yrs)', 'Elderly (65yrs-above)') ,
						'post_status' => 'publish',
						'suppress_filters' => true
						 );
					$recent_posts = wp_get_recent_posts( $recent_args );


					foreach( $recent_posts as $recent ){ ?>
						<div class="items d-flex align-middle items flex-column">
							
							<?php
							// fetch title and id
							$post = get_post( $recent["ID"] ); 
							$title = $post->post_title;

							// make the_content work outside of the main loop
							$id =  get_the_ID( $recent );
							$p = get_post($id);
							$text = $p->post_content;
							$text = strip_shortcodes( $text );
							$text = apply_filters( 'the_content', $text );
							$text = str_replace(']]>', ']]&gt;', $text);
							$excerpt_length = apply_filters( 'excerpt_length', 55 );
							$excerpt_more = '<a class="btn btn-primary read-more mt-auto p-1" href="'. get_permalink($id) . '">Read More</a>';
							$text = wp_trim_words( $text, $excerpt_length );
							$categories = get_the_category($id);
							$category_id = get_cat_ID( $categories[0]->name);
							$category_link = get_category_link( $category_id );  ?>

							<a class="carousel-link d-flex flex-column align-items-stretch" href="<?php the_permalink()?>">
								<div class="thumbnail p-1 item">
									<?php if ( has_post_thumbnail()) { ?>
										<?php the_post_thumbnail('large');
									} else { ?>
										<img class="" src="<?php echo get_template_directory_uri(); ?>/dist/images/foot-blue.svg"/>
									<?php  }; ?>
								</div>
								
								<h4 class="card-title p-1"><?php echo $title ?></h4>
								<div class="p-1 category-tags-holder">
									<div class="category-holder">
										<?php if ( $categories ) { ?>
											<div class="category-holder-title"> Inside Category: </div>
											<?php foreach( $categories as $cat ) { 

												$category_id = get_cat_ID( $cat->name ); ?>
												<div class="category"><?php echo esc_html( $cat->name ); ?></div>
											
											<?php }
										} else { ?>
											<div class="category-holder-title"> Inside Category: </div>
											<div class="category">Article has no Category</div>	
										<?php } ?>
									</div>
									<div class="tags-holder">	
										<?php if ( $post_tags ) { ?>
											<div class="tags-holder-title"> Inside Tags: </div>
											<?php foreach( $post_tags as $tag ) { ?>
											<div class="tag"><?php echo esc_html( $tag->name ) ?></div>
											<?php }
										} else { ?>
											<div class="tags-holder-title"> Inside Tags: </div>
											<div class="tag">Article has no Tags</div>
										<?php } ?>
									</div>
								</div>
								<p class="p-1"><?php echo $text ?> </p>
								<div class="btn mt-auto p-1"s>Read More</div>
							</a>
						</div>
					<?php };
				wp_reset_query(); ?>
				</div>
			</div>

		</div>
	</div>

	<div id="participate-in-study" class="row justify-content-md-center relative"> 
		<div class="area-divider col col-md-4">
		</div>
		<div class="cat-title col">
			<h3>Participate</h3>
		</div>
		<div class="area-divider col col-md-4">
		</div>
		<div class="row my-auto justify-content-around">
			
			<div class="col-12 col-lg-4 col-xl-3 d-flex align-middle items flex-column" >
				<?php 
					$page_title = get_page_by_title("Participate"); 
					$post = get_post($page_title); 
					$link = get_permalink($post);
					$post_title = get_the_title($post) ; ?>

					<a class="carousel-link d-flex flex-column align-items-stretch align-self-stretch" href="<?php echo $link ?>">

						<div class="p-1 text-center thumbnail">
							<?php if ( has_post_thumbnail()) { ?>
								<?php the_post_thumbnail();
							} else { ?>
								<p class="fa fa-handshake-o" aria-hidden="true"></p>
							<?php  }; ?>
						</div>
						<?php echo '<h5 class="area-title p-1">' . $post_title . '</h5>'?>
						<p class="p-1"><?php echo $post->post_excerpt ; ?></p>
						<div class="btn mt-auto p-1" href="<?php echo  $link ?>">Read More</div>
					</a>
				<?php wp_reset_query(); ?>
			</div>

			<div class="col-12 col-lg-4 d-flex align-middle items flex-column">
					<?php
					$page_title = get_page_by_title("Question to Nurture Science"); 
					$post = get_post($page_title); 
					$link = get_permalink($post);
					$post_title = get_the_title($post) ; ?>

					<a class="carousel-link d-flex flex-column align-items-stretch" href="<?php echo $link ?>">
						<div class="p-1 text-center thumbnail">
							<?php if ( has_post_thumbnail()) { ?>
								<?php the_post_thumbnail();
							} else { ?>
								<p class="fa fa-question-circle-o" aria-hidden="true"></p>
							<?php  }; ?>
						</div>
						<?php echo '<h5 class="area-title p-1">' . $post_title . '</h5>'?>
						<p class="p-1"><?php echo $post->post_excerpt ; ?></p>
						<div class="btn mt-auto p-1" href="<?php echo  $link ?>">Read More</div>
					</a>
				<?php wp_reset_query(); ?>
			</div>

			
			<div class="col-12 col-lg-4 col-xl-3  d-flex align-middle items flex-column">
				<?php 
					$page_title = get_page_by_title("Donate to this site"); 
					$post = get_post($page_title); 
					$link = get_permalink($post);
					$post_title = get_the_title($post) ; ?>

					<a class="carousel-link d-flex flex-column align-items-stretch" href="<?php echo $link ?>">
						<div class="p-1 text-center thumbnail">
							<?php if ( has_post_thumbnail()) { ?>
								<?php the_post_thumbnail();
							} else { ?>
								<p class="fa fa-money" aria-hidden="true"></p>
							<?php  }; ?>
						</div>
						<?php echo '<h5 class="area-title p-1">' . $post_title . '</h5>'?>
						<p class="p-1"><?php echo $post->post_excerpt ; ?></p>
						<div class="btn mt-auto p-1" href="<?php echo  $link ?>">Read More</div>
					</a>
				<?php wp_reset_query(); ?>
			</div>
		</div>
	</div>

	<div id="social-area" class="row justify-content-md-center relative">
		<div class="area-divider col col-md-4">
		</div>
		<div class="cat-title col">
			<h3>Social</h3>
		</div>
		<div class="area-divider col col-md-4">
		</div>
		<div class="row my-auto justify-content-around">
			<div class="col-12 col-md-5 col-xl-5 d-flex align-middle items flex-column">
				<?php
				$page_title = get_page_by_title("YouTube Channel"); 
				$post = get_post($page_title); 
				$link = get_permalink($post);
				$post_title = get_the_title($post) ; ?>
				<a class="carousel-link d-flex flex-column align-items-stretch" href="<?php echo $link ?>">
					<?php echo '<h5 class="area-title p-1">' . $post_title . '</h5>';
					if ( has_post_thumbnail()) { ?>
					<?php the_post_thumbnail();
					} else { ?>
					<p class="fa fa-youtube" aria-hidden="true"></p>

					<?php  }; ?>
					<p class="p-1"><?php echo $post->post_excerpt ; ?></p>
					<div class="btn mt-auto p-1" href="<?php echo  $link ?>">Read More</div>
					<?php wp_reset_query(); ?>
				</a>
			</div>

			<div class="col-12 col-md-5 col-xl-5 d-flex align-middle items flex-column">
				<?php
				$page_title = get_page_by_title("Facebook"); 
				$post = get_post($page_title); 
				$link = get_permalink($post);
				$post_title = get_the_title($post) ; ?>
				<a class="carousel-link d-flex flex-column align-items-stretch" href="<?php echo $link ?>">
					<?php echo '<h5 class="area-title p-1">' . $post_title . '</h5>';
					if ( has_post_thumbnail()) { ?>
					<?php the_post_thumbnail();
					} else { ?>
					<p class="fa fa-facebook-official" aria-hidden="true"></p>

					<?php  }; ?>
					<p class="p-1"><?php echo $post->post_excerpt ; ?></p>
					<div class="btn mt-auto p-1" href="<?php echo  $link ?>">Read More</div>
					<?php wp_reset_query(); ?>
				</a>
			</div>
		</div>
	</div>

    <div id="signUp"  class="row justify-content-md-center relative">
		<div class="area-divider col col-md-4">
		</div>
		<div class="cat-title col">
			<h3>Sign Up</h3>
		</div>
		<div class="area-divider col col-md-4">
		</div>
		<div class="row my-auto justify-content-around">
			<div class="col-12 col-md-5 d-flex align-middle items flex-column" >
				<?php 
				$post = get_post($page_title); 
				$link = get_permalink($post);
				$post_title = get_the_title($post) ; ?>
				<a class="carousel-link d-flex flex-column align-items-stretch" href="<?php echo $link ?>">
					<?php echo '<h5 class="area-title p-1">' . $post_title . '</h5>'; ?>

					<?php if ( has_post_thumbnail()) { ?>
						<?php the_post_thumbnail();
						} else { ?>
						<p class="fa fa-address-book" aria-hidden="true"></p>

					<?php  }; ?>
					<p class="cp-1"><?php echo $post->post_excerpt ; ?></p>
					<div class="btn mt-auto p-1" href="<?php echo  $link ?>">Read More</div>
					<?php wp_reset_query(); ?>
				</a>
			</div>
			<div class="col-12 col-md-5 d-flex align-middle items flex-column" >
				<?php
				$page_title = get_page_by_title("Sign up for article alerts"); 
				$post = get_post($page_title); 
				$link = get_permalink($post);
				$post_title = get_the_title($post) ; ?>
				<a class="carousel-link d-flex flex-column align-items-stretch" href="<?php echo $link ?>">
						<?php echo '<h5 class="area-title p-1">' . $post_title . '</h5>'; ?>
						<?php if ( has_post_thumbnail()) { ?>
						<?php the_post_thumbnail();
						} else { ?>
							<p class="fa fa-pencil" aria-hidden="true"></p>

						<?php  }; ?>
					<p class="p-1"><?php echo $post->post_excerpt ; ?></p>
					<div class="btn mt-auto p-1" href="<?php echo  $link ?>">Read More</div>
					<?php wp_reset_query(); ?>
				</a>
			</div>
		</div>
	</div>
</div>






<?php endwhile; ?>
