<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>
	<h1> Response tags archive</h1>
	<div class="card-columns">
		<?php while (have_posts()) : the_post(); ?>
			<article class="card" <?php post_class(); ?> >
			  <header>
			  	<?php if ( has_post_thumbnail() ) : ?>
				    <div class="card-img-top">
				        <?php the_post_thumbnail(); ?>
				    </div>
				<?php endif; ?>
			  	<div class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			    <?php get_template_part('templates/entry-meta'); ?>
			  </header>
			  <div class="entry-summary card-text">
			    <?php the_excerpt(); ?>
			  </div>
			</article>
		<?php endwhile; ?>
	</div>
<?php the_posts_navigation(); ?>
